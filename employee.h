#include <string>

#ifndef EMPLOYEE_H
#define EMPLOYEE_H

#endif // EMPLOYEE_H

class employee
    {
 public:
     employee ();
     std::string getName();
     void setName(std::string _name);
     int getGender();
     void setGender(int _gender);
     float getNsh();
     void setNsh(float _nsh);
private:
    std::string Name_str;
    int Gender_int;
    float Nsh_flo;
  };
